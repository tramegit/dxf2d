##------------------------------------------------------------------------------
##  Simplified BSD License
##  Copyright (c) 2022 PAULO C. ORMONDE
## 
##  Redistribution and use in source and binary forms, with or without
##  modification, are permitted provided that the following conditions
##  are met:
##  1. Redistributions of source code must retain the above copyright
##     notice, this list of conditions and the following disclaimer.
##  2. Redistributions in binary form must reproduce the above copyright
##     notice, this list of conditions and the following disclaimer in the
##     documentation and/or other materials provided with the distribution.
## 
##  THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS "AS IS" AND
##  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
##  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
##  ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
##  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
##  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
##  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
##  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
##  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
##  OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
##  SUCH DAMAGE.
## 
##  DXF2D 22.06
##------------------------------------------------------------------------------

% dxf2d - Drawing a simple 3D frame

% Start global variables
 startdxf2d; 
 
% filename without extension
 newdxf('example3d')
 
% Drawing lines for columns   
   layer='columns'; color='1';  %set layer and color
   addline3d(  0,    0,  0,    0,    0, 500)
   addline3d(300,    0,  0,  300,    0, 500)
   addline3d(300,  300,  0,  300,  300, 500)
   addline3d(  0,  300,  0,    0,  300, 500)
   
##% Drawing beans
##   layer='beans'; color='7';  % set layer and color
##   addrect(-50,-50,50,50)
##
##% Drawing text    
##   layer='Texto';  color='4';      % set layer and color
##   addtext(0, 3000, 800,'DXF2D', 0)
##
##% Drawing arcs   
##   layer='Arco';  color='6';       % set layer and color
##   addarc(3000,3000,1000,0,180)
##   addarc(-3000,3000,1000,0,180)
##
##% Drawing by loop  
##      for i = [1:10]
##        layer='Rectangles'; color='3';       % set layer and color
##        addrect(-200*i,-200*i,200*i,200*i)
##        color=int2str(i);
##        addcircle(0,-3000,100*i);            % Drawing circles
##      endfor
##
##% Drawing solid entities      
##      layer='Solid'; color='252';   % set layer and color
##      addsolid(4000,0,2000,3000) 
##      addsolid(-4000,0,-2000,3000)
##
##% Drawing dimensions     
##      dimsize=100; layer='Dim'; color=1;   % set dimension size, layer and color
##      hdim(2000,0,4000,-500)               % Horizontal dimensions
##      hdim(-4000,0,-2000,-500)
##      vdim(4000,0,4500,3000)               % Vertical dimensions
##      vdim(-4000,0,-4500,3000)
      
% Close the file and save the drawing
 enddxf