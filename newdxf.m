##------------------------------------------------------------------------------
##  Simplified BSD License
##  Copyright (c) 2022 PAULO C. ORMONDE
## 
##  Redistribution and use in source and binary forms, with or without
##  modification, are permitted provided that the following conditions
##  are met:
##  1. Redistributions of source code must retain the above copyright
##     notice, this list of conditions and the following disclaimer.
##  2. Redistributions in binary form must reproduce the above copyright
##     notice, this list of conditions and the following disclaimer in the
##     documentation and/or other materials provided with the distribution.
## 
##  THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS "AS IS" AND
##  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
##  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
##  ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
##  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
##  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
##  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
##  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
##  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
##  OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
##  SUCH DAMAGE.
## 
##  DXF2D 22.09
##------------------------------------------------------------------------------

% Create a dxf file at current directory
%
% Usage: newdxf(name)
%
function newdxf(name)
 global dxffile fid

#startdxf2d;  

dxffile = strcat(name , '.dxf');
fid = fopen (dxffile, "w");

%initial section
fdisp (fid, '999');
fdisp (fid, 'DXF created from TRAMETOOLS');

# HEADER SECTION
fdisp (fid, '0');
fdisp (fid, 'SECTION');
fdisp (fid, '2');
fdisp (fid, 'HEADER');
fdisp (fid, '9');
fdisp (fid, '$ACADVER');
fdisp (fid, '1');
fdisp (fid, 'AC1006');
fdisp (fid, '9');
fdisp (fid, '$INSBASE');
fdisp (fid, '10');
fdisp (fid, '0.0');
fdisp (fid, '20');
fdisp (fid, '0.0');
fdisp (fid, '30');
fdisp (fid, '0.0');
fdisp (fid, '9');
fdisp (fid, '$EXTMIN');
fdisp (fid, '10');
fdisp (fid, '0.0');
fdisp (fid, '20');
fdisp (fid, '0.0');
fdisp (fid, '9');
fdisp (fid, '$EXTMAX');
fdisp (fid, '10');
fdisp (fid, '1000.0');
fdisp (fid, '20');
fdisp (fid, '1000.0');
fdisp (fid, '0');
fdisp (fid, 'ENDSEC');

% BLOCK SECTION
fdisp (fid, '0');
fdisp (fid, 'SECTION');
fdisp (fid, '2');
fdisp (fid, 'BLOCKS');
fdisp (fid, '0');
fdisp (fid, 'ENDSEC');

% ENTITIES SECTION
fdisp (fid, '0');
fdisp (fid, 'SECTION');
fdisp (fid, '2');
fdisp (fid, 'ENTITIES');
	
endfunction
