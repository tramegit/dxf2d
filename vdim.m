##------------------------------------------------------------------------------
##  Simplified BSD License
##  Copyright (c) 2022 PAULO C. ORMONDE
## 
##  Redistribution and use in source and binary forms, with or without
##  modification, are permitted provided that the following conditions
##  are met:
##  1. Redistributions of source code must retain the above copyright
##     notice, this list of conditions and the following disclaimer.
##  2. Redistributions in binary form must reproduce the above copyright
##     notice, this list of conditions and the following disclaimer in the
##     documentation and/or other materials provided with the distribution.
## 
##  THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS "AS IS" AND
##  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
##  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
##  ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
##  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
##  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
##  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
##  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
##  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
##  OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
##  SUCH DAMAGE.
## 
##  DXF2D 22.06
##------------------------------------------------------------------------------

% Drawing a vertical dimension with the current layer, color, and size.
%
% Usage: vdim(xi, yi, xf, yf)  
%
function vdim(xp1, yp1, xp2, yp2)

global fid layer color dimsize

  if yp1 < yp2 
      ypa= yp1 - 0.5 * dimsize;
      ypb= yp2 + 0.5 * dimsize;
    else
      ypa= yp1 + 0.5 * dimsize;
      ypb= yp2 - 0.5 * dimsize;
    endif

    radius = 0.3 * dimsize;
    ypm = mean([yp1 yp2]);
    txt = distance(0, yp1, 0, yp2);
    
  if xp1 < xp2	
      xpa= xp1 + 0.60 * dimsize;
      xpb= xp2 + 0.60 * dimsize;
        addline(xpa, yp1, xpb, yp1);
        addline(xpa, yp2, xpb, yp2);
        addline(xp2, ypa, xp2, ypb);
        addcircle(xp2, yp1, radius);
        addcircle(xp2, yp2, radius);
      xpm = xp2 - 0.75 * dimsize;
    addtext(xpm, ypm, dimsize, txt, 90);
  else
      xpa= xp1 - 0.60 * dimsize;
      xpb= xp2 - 0.60 * dimsize;
        addline(xpa, yp1, xpb, yp1);
        addline(xpa, yp2, xpb, yp2);
        addline(xp2, ypa, xp2, ypb);
        addcircle(xp2, yp1, radius);
        addcircle(xp2, yp2, radius);
      xpm = xp2 - 0.75 * dimsize;
    addtext(xpm, ypm, dimsize, txt, 90);
  endif
endfunction
