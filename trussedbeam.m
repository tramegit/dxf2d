##------------------------------------------------------------------------------
##  Simplified BSD License
##  Copyright (c) 2022 PAULO C. ORMONDE
## 
##  Redistribution and use in source and binary forms, with or without
##  modification, are permitted provided that the following conditions
##  are met:
##  1. Redistributions of source code must retain the above copyright
##     notice, this list of conditions and the following disclaimer.
##  2. Redistributions in binary form must reproduce the above copyright
##     notice, this list of conditions and the following disclaimer in the
##     documentation and/or other materials provided with the distribution.
## 
##  THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS "AS IS" AND
##  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
##  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
##  ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
##  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
##  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
##  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
##  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
##  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
##  OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
##  SUCH DAMAGE.
## 
##  DXF2D 22.06
##------------------------------------------------------------------------------

% Draw a trussed beam
%
% Usage: trussedbeam (L, H, NP, xo, yo)
% The xo and yo (origin coordinates) are optional.

function trussedbeam(L, H, NP, xo=0, yo=0)
  global fid layer color
  
  if ( mod(NP,2) == 0 ) %Euclidean division
    
    x = 0:NP:L;
    LP = L / NP;
    xi=xo; 
    yi=yo;
    yf=yo+H;
    
    for i = 1:NP 
      xf = xi + LP;
         
         % Drawing bottom chord 
         layer='Bottom chord'; color='1';  
         addline(xi,yi,xf,yi)
         
         % Drawing top chord 
         layer='Top chord'; color='2';  
         addline(xi,yf,xf,yf)

         if ( mod(i,2) == 0 ) %Euclidean division
                  % Drawing Diagonal
                  layer='Diagonal'; color='4'; 
                  addline(xi,yf,xf,yi)                 
           else
                  % Drawing Diagonal
                  layer='Diagonal'; color='4';  
                  addline(xi,yi,xf,yf)
         endif
      
      xi = xf;  
      
           if i < NP   
                 % Drawing post
                 layer='Post'; color='3';  
                 addline(xi,yi,xi,yf)
           endif
    end
  endif
  
% Drawing End Posts
layer='End Post'; color='5';  
addline(xo,yo,xo,yf)
addline(xo+L,yo,xo+L,yf) 
  
endfunction