# DXF2D 
### Version 22.09
![](viga2d.png)
[Octave](https://www.gnu.org/software/octave/index) functions to create drawings in DXF format.


## Global Variables

### dxffile
Name of the current file. The default name at load is <i>temp.dxf</i>.

### layer
Set the current layer

``` octave
layer = 'The Doors';
```
  
### color
Set the current color. Use **AutoCAD Index Color** (numbers between 1 and 255).

``` octave
color = '1';
```
  
### dimsize
Set the current dimension size. Consider dimsize as a float point number.

``` octave
dimsize = 100;
```
  
   
## Functions to Start, Open and Close a DXF file.

### startdxf2d
Start global variables

``` octave
startdxf2d;
```
  

### newdxf
Create a DXF file by the filename without extension

``` octave
newdxf('example')
```
 
### enddxf
Just close the file and save the drawing with the DXF extension

``` octave
enddxf;
```

## Functions to Add Drawing Entities.
 
### addline
Add lines with the current layer and color<br>
Usage: addline(xi, yi, xf, yf)

``` octave 
addline (0, 0, 100, 100);
```

### addline3d
Add lines with the current layer and color<br>
Usage: addline(xi, yi, zi, xf, yf, zf)

``` octave 
addline3d (0, 0, 0, 100, 100, 100);
```


### addrect
Add rectangles with the current layer and color<br>
Usage: <br> addrect (xi, yi, xf, yf)

``` octave 
addrect (0, 0, 100, 100);
```
 
### addcircle
Add circles with the current layer and color<br>
Usage: <br> addcircle (x, y, radius)

``` octave 
addcircle (50, 50, 50);
```  

### addarc
Add arc with the current layer and color. Drawing counterclockwise.
Usage: <br> addarc (x, y, radius, start_angle, end_angle)
Where x and y are the center point coordinates.

``` octave 
addarc (100, 0, sqrt(2)*50, 90, 180);
```  

### addsolid
Add solid rectangles with the current layer and color. Drawing by corner coordinates. <br>
Usage: <br> addsolid (xi, yi, xf, yf)

``` octave 
addsolid (0, 110, 100, 120);
```  


### addtext
Add text with the current layer and color. <br>
Where x and y are the center point coordinates.<br>
Usage: <br> addtext (x, y, height, text, angle)

``` octave 
addtext (50, 150, 25, 'DXF2D', 0);
```  


### hdim
Drawing a horizontal dimension with the current layer, color, and size. <br>
Drawing by corner coordinates. <br>
Usage: <br> hdim (xi, yi, xf, yf)

``` octave 
dimsize = 15;
hdim (0, 0, 100, -50);
```  


### vdim
Drawing a vertical dimension with the current layer, color, and size. <br>
Drawing by corner coordinates. <br>
Usage: <br> vdim (xi, yi, xf, yf)

``` octave 
dimsize = 20;
vdim (100, 0, 150, 100);
```


## Symbols
### add_u
Draw three lines in the shape of a 'U' with the height 'h'. <br>

Usage: add_u (xi, yi, xf, yf, h)	

``` octave
add_u(0, 0, 100, 0, 50)  
```

### add_ue
Draw three lines in the shape of a 'UE' with the height 'h' and fold 'f'. <br>

Usage: add_ue (xi, yi, xf, yf, h, f)	

``` octave
add_ue(0, 0, 100, 0, 50, 25) 

```

### centermark
Center mark or plan level symbol <br>
Usage: <br> **centermark** (xi, yi, radius)

``` octave 
centermark(0, 0, 100);
```

### addsection
Section line <br>
Usage: <br> **addsection** (xi, yi, xf, yf, secsize, chr='A') <br>

``` octave 
addsection(0, 0, 1000, 0, 100, 'X');
```



## General functions.


### distance
It calculates the distance between two points.<br>
Usage: distance (x1, y1, x2, y2)

``` octave
distance(0, 0, -1, 1)  %return 1.4142
```

### getangle
It calculates the angle between two points.<br>
Usage: angle (x1, y1, x2, y2)

``` octave
getangle(0, 0, -1, 1)  %return 135
```

### point_from
It returns the coordinates of the target point from the origin point.<br>
Usage: point_from (x, y, distance, rotation)

``` octave
[x,y] = point_from(0, 0, 1, 30)  % x = 0.8660
                         % y = 0.5000
```

### midpoint
This function returns the midpoint coordinates.<br>

Usage: midpoint (xi, yi, xf, yf)

``` octave
[x, y] = midpoint(0, 0, 4, 3)  % x = 2
                               % y = 1.5000
```



## Example 1 - Quickstart
![](quickstart.png)

``` octave
% Quickstart example
%Set Global Variables
startdxf2d

%Create a DXF file → quickstart.dxf
newdxf('quickstart')

%Create drawing entities supported by DXF2D
  addline (0, 0, 100, 100);
  addrect (0, 0, 100, 100);
  addcircle (50, 50, 50);
  addarc (100, 0, sqrt(2)*50, 90, 180);
  addsolid (0, 110, 100, 120);
  addtext (50, 150, 25, 'DXF2D', 0);
  dimsize = 15;
  hdim (0, 0, 100, -50);
  vdim (100, 0, 150, 100);

% Close and save quickstart.dxf
enddxf
```

## Example 2
![](example.png)

No need for installation, just enter the folder where you saved the functions and run the example.m inside [Octave](https://www.gnu.org/software/octave/index). Use the example below to create different drawings or new functions.

You can also set functions path to create drawings in different folders using **addpath** function. <br>
Example:
addpath('/home/user/dxf2d")


``` octave
% dxf2d - Drawing example

% Set functions path (optional)
% addpath('~/trametools/dxf2d');

% Start global variables
 startdxf2d; 
 
% filename without extension
 newdxf('exemplo')
 
% Drawing lines    
   layer='Lines'; color='1';  %set layer and color
   addline(-50,-50,50,50)
   addline(50,-50,-50,50)
   
% Drawing rectangle
   layer='Rectangles'; color='7';  % set layer and color
   addrect(-50,-50,50,50)

% Drawing text    
   layer='Texto';  color='4';      % set layer and color
   addtext(0, 3000, 800,'DXF2D', 0)

% Drawing arcs   
   layer='Arco';  color='6';       % set layer and color
   addarc(3000,3000,1000,0,180)
   addarc(-3000,3000,1000,0,180)

% Drawing by loop  
      for i = [1:10]
        layer='Rectangles'; color='3';       % set layer and color
        addrect(-200*i,-200*i,200*i,200*i)
        color=int2str(i);
        addcircle(0,-3000,100*i);            % Drawing circles
      endfor

% Drawing solid entities      
      layer='Solid'; color='252';   % set layer and color
      addsolid(4000,0,2000,3000) 
      addsolid(-4000,0,-2000,3000)

% Drawing dimensions     
      dimsize=100; layer='Dim'; color=1;   % set dimension size, layer and color
      hdim(2000,0,4000,-500)               % Horizontal dimensions
      hdim(-4000,0,-2000,-500)
      vdim(4000,0,4500,3000)               % Vertical dimensions
      vdim(-4000,0,-4500,3000)
      
% Close the file and save the drawing
 enddxf
```


## License

[opensource.org](https://opensource.org/licenses/BSD-2-Clause/)

```

  Simplified BSD License
  Copyright (c) 2022 PAULO C. ORMONDE
  All rights reserved.
 
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:
  1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
 
  THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
  OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
  SUCH DAMAGE.
 
  DXF2D 22.06

```


