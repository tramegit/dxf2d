##------------------------------------------------------------------------------
##  Simplified BSD License
##  Copyright (c) 2022 PAULO C. ORMONDE
## 
##  Redistribution and use in source and binary forms, with or without
##  modification, are permitted provided that the following conditions
##  are met:
##  1. Redistributions of source code must retain the above copyright
##     notice, this list of conditions and the following disclaimer.
##  2. Redistributions in binary form must reproduce the above copyright
##     notice, this list of conditions and the following disclaimer in the
##     documentation and/or other materials provided with the distribution.
## 
##  THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS "AS IS" AND
##  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
##  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
##  ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
##  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
##  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
##  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
##  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
##  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
##  OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
##  SUCH DAMAGE.
## 
##  DXF2D 22.06
##------------------------------------------------------------------------------

% Draw three lines in the shape of a 'UE' with the height 'h' and fold 'f'.
%
% Usage: add_ue(xi, yi, xf, yf, h, f)

function add_ue(x1, y1, x2, y2, h, f)
  global fid layer color
    angu = getangle(x1,y1,x2,y2);
    addline ( x1, y1, x2, y2 );
    [xi1,yi1] = point_from(x1, y1, h, angu+90);
    [xf1,yf1] = point_from(x2, y2, h, angu+90); 
    addline ( x1, y1, xi1, yi1 );
    addline ( x2, y2, xf1, yf1 );
    [xi2,yi2] = point_from(xi1, yi1, f, angu);
    [xf2,yf2] = point_from(xf1, yf1, f, angu+180);
    addline ( xi1, yi1, xi2, yi2 );
    addline ( xf1, yf1, xf2, yf2 );
endfunction
