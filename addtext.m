##------------------------------------------------------------------------------
##  Simplified BSD License
##  Copyright (c) 2022 PAULO C. ORMONDE
## 
##  Redistribution and use in source and binary forms, with or without
##  modification, are permitted provided that the following conditions
##  are met:
##  1. Redistributions of source code must retain the above copyright
##     notice, this list of conditions and the following disclaimer.
##  2. Redistributions in binary form must reproduce the above copyright
##     notice, this list of conditions and the following disclaimer in the
##     documentation and/or other materials provided with the distribution.
## 
##  THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS "AS IS" AND
##  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
##  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
##  ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
##  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
##  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
##  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
##  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
##  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
##  OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
##  SUCH DAMAGE.
## 
##  DXF2D 22.06
##------------------------------------------------------------------------------

% Add text with current layer and color
%
% Usage: addtext (x, y, height, text, angle)
% Where x and y are the center point coordinate.
%
function addtext (x, y, height, txt, ang, align='center')
 global fid layer color
    fdisp (fid, '0' );
    fdisp (fid, 'TEXT');
    fdisp (fid, '8');
    fdisp (fid, layer);
    fdisp (fid, '62' );
    fdisp (fid, color);
    fdisp (fid, '40' );
    fdisp (fid, height );
    fdisp (fid, '1' );
    fdisp (fid, txt );
    fdisp (fid, '10' );
    fdisp (fid, x );
    fdisp (fid, '20' );
    fdisp (fid, y );
    fdisp (fid, '30' );
    fdisp (fid, '0.0' );
    fdisp (fid, '50' );
    fdisp (fid, ang );
    fdisp (fid, '72' );
       switch align
         case 'center'
           fdisp (fid, '1' );
         case 'left'
           fdisp (fid, '0' );
         case 'right'
           fdisp (fid, '2' );
       endswitch
    fdisp (fid, '11' );
    fdisp (fid, x );
    fdisp (fid, '21' );
    fdisp (fid, y );
    fdisp (fid, '31' );
    fdisp (fid, '0.0' );
    fdisp (fid, '73' );
    fdisp (fid, '2' );
endfunction
