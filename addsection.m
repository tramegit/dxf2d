
##------------------------------------------------------------------------------
##  Simplified BSD License
##  Copyright (c) 2022 PAULO C. ORMONDE
##
##  Redistribution and use in source and binary forms, with or without
##  modification, are permitted provided that the following conditions
##  are met:
##  1. Redistributions of source code must retain the above copyright
##     notice, this list of conditions and the following disclaimer.
##  2. Redistributions in binary form must reproduce the above copyright
##     notice, this list of conditions and the following disclaimer in the
##     documentation and/or other materials provided with the distribution.
##
##  THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS "AS IS" AND
##  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
##  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
##  ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
##  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
##  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
##  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
##  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
##  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
##  OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
##  SUCH DAMAGE.
##
##  DXF2D 22.06
##------------------------------------------------------------------------------

% Add a section line with using layer "section" and colors 6 (0.6mm) and 1 (0.1 mm)
function addsection(xi, yi, xf, yf, secsize, chr='A')
  global fid layer color

  %linha de corte
  layer='section'; color='1';
  addline(xi,yi,xf,yf);

  %seta inicial
  rot = getangle(xi,yi,xf,yf);
  [x1,y1]=point_from(xi,yi,secsize,rot+90);
  [x11,y11]=point_from(x1,y1,secsize/4,rot+90-135);
  [x12,y12]=point_from(x1,y1,secsize/4,rot+90+135);
  addline(x1,y1,x11,y11);
  addline(x1,y1,x12,y12);
  addline(xi,yi,x1,y1);

  %texto inicial
  [xt,yt]=point_from(x1,y1,secsize/2,rot+90);
  addtext(xt,yt,secsize/2,chr,rot);

  %seta final
  [x1,y1]=point_from(xf,yf,secsize,rot+90);
  [x11,y11]=point_from(x1,y1,secsize/4,rot+90-135);
  [x12,y12]=point_from(x1,y1,secsize/4,rot+90+135);
  addline(x1,y1,x11,y11);
  addline(x1,y1,x12,y12);
  addline(xf,yf,x1,y1);

    %texto final
  [xt,yt]=point_from(x1,y1,secsize/2,rot+90);
  addtext(xt,yt,secsize/2,chr,rot);

endfunction

